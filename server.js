const http = require('http');
const port = 1101;
const hostname = "127.0.0.1";
const app = require('./app');
const server = http.createServer(app);

server.listen(port, hostname, (err, res) => {
    if (err) {
        return console.log("Failed to connect to the db");
    }
    console.log("API started listening on the port: " + port);
});
