const express = require('express');
const app = express();
const bodyParser = require('body-parser');
// const mongoose = require('mongoose');
const cors = require('cors');
const routes = require('./routes/routes');
const morgan = require('morgan');
// const mongodbPort = "mongodb://localhost:27017/vscapp";

app.use(cors());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(morgan('dev'));

// mongoose.connect(mongodbPort, {
//     useNewUrlParser: true
// }).then(() => {
//         console.log("Successfully connected to the db...");
//     },
//     (err) => {
//         console.log("Error while connecting: " + err);
//     }
// );

app.use('/', routes);
module.exports = app;