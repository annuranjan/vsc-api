const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
//password = "123abc"
const username = "user";
const password = "$2a$10$tQ6BNXuZr8TdKioNLtM7ZOqQPXSlzQXHaLB10JgXRpIfL72i.MM.C";

router.post("/login", (req, res) => {
    console.log(req.body);
    if (!req.body.password || !req.body.username) {
        return res.status(400).json({
            "message": "Both Password and Username are required!"
        });
    }
    if (req.body.username !== username) {
        return res.status(401).json({
            "message": "Wrong username/password"
        });
    }
    console.log("req-body-password and password: ", req.body.password, password);
    bcrypt.compare(req.body.password, password, (err, response) => {
        console.log("response", response);
        if (err) {
            console.log(err);
            return res.status(500).json({
                "message": "Something went wrong!"
            })
        }
        if (!response) {
            console.log("Wrong password");
            return res.status(401).json({
                "message": "Wrong username/password"
            });
        }
        return res.status(200).send({
            token: jwt.sign({
                username: username
            }, "ThisIsSuperDuperSecretKey", {
                expiresIn: '10m'
            }),
            username: username
        });
    })
});

module.exports = router;